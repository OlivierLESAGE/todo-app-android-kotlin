package fr.optimergo.todoapp

abstract class OnClickItemListener<T> {
    abstract fun onClick(data: T)
}