package fr.optimergo.todoapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.optimergo.todoapp.databinding.ItemTodoBinding

class TodoListAdapter(private val itemClickListener: OnClickItemListener<Todo>) :
    ListAdapter<Todo, TodoListAdapter.TodoViewHolder>(TODOS_COMPARATOR) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        return TodoViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.bind(getItem(position), itemClickListener)
    }

    class TodoViewHolder(
        private val itemBinding: ItemTodoBinding,
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(todo: Todo, listener: OnClickItemListener<Todo>) {
            itemBinding.todo = todo
            itemBinding.card.setOnClickListener {
                listener.onClick(todo)
            }
        }

        companion object {
            fun create(parent: ViewGroup): TodoViewHolder {
                val binding =
                    ItemTodoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return TodoViewHolder(binding)
            }
        }
    }

    companion object {
        private val TODOS_COMPARATOR = object : DiffUtil.ItemCallback<Todo>() {
            override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(
                oldItem: Todo,
                newItem: Todo
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

}