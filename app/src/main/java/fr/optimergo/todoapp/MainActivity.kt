package fr.optimergo.todoapp

import android.graphics.Rect
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import fr.optimergo.todoapp.databinding.ActivityMainBinding
import fr.optimergo.todoapp.databinding.LayoutDialogBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val todoViewModel: TodoViewModel by viewModels {
        TodoViewModelFactory((application as TodoApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val listAdapter = TodoListAdapter(object : OnClickItemListener<Todo>() {
            override fun onClick(data: Todo) {
                todoViewModel.update(data, !data.done)
            }
        })

        with(binding.recyclerView) {
            adapter = listAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    itemPosition: Int,
                    parent: RecyclerView
                ) {
                    with(outRect) {
                        top = 16
                        left = 16
                        right = 16
                        bottom = 16
                    }
                }
            })
        }

        todoViewModel.allTodos.observe(this, Observer { todos ->
            todos.let { listAdapter.submitList(todos) }
        })

        binding.addButton.setOnClickListener {
            val dialogBinding = LayoutDialogBinding.inflate(layoutInflater)
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.create_todo))
                .setView(dialogBinding.root)
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                    // Respond to neutral button press
                }
                .setPositiveButton(resources.getString(R.string.create)) { dialog, which ->
                    todoViewModel.insert(Todo(dialogBinding.title.editText?.text.toString()))
                }
                .show()
        }
    }


}