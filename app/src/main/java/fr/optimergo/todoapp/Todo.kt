package fr.optimergo.todoapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "todo_table")
data class Todo(
    @PrimaryKey @ColumnInfo(name = "todo") val todo: String,
    @ColumnInfo(name = "done") var done: Boolean = false,
    @ColumnInfo(name = "created_at") val createdAt: Date = Date()
)