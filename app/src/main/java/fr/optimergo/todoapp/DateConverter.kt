package fr.optimergo.todoapp

import android.util.Log
import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    @TypeConverter
    fun fromText(value: String): Date {
        Log.d("CONVERTERS", value)
        Log.d("CONVERTERS", Date(value).toString())
        return Date(value)
    }

    @TypeConverter
    fun toText(date: Date): String {
        Log.d("CONVERTERS", date.toString())
        return date.toString()
    }
}